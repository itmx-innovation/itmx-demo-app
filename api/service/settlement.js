
/**
 * SettlementService communicate to blockchain network in order to 
 * make a query or invoke chaincode
 */

const Client = require('fabric-client');
const path = require('path');
const store_path = path.join(__dirname, '../hfc-key-store');

const queryByDateFnc ='queryByDate';
const getTransactionFnc = 'getTransaction';
const settleTransactionFnc = 'settleTransaction';

const chaincodeId = 'bulkpayment';


function SettlementService(channelId, peerUri, ordererUri){
    this.user = null;
    this.event_hub = null;
    this.regid = null;
    this.client = new Client();
    this.channel = this.client.newChannel(chaincodeId);

    let peer = this.client.newPeer(peerUri);
    this.channel.addPeer(peer);
    console.debug(`Connected to peer: ${peerUri}`);

    let orderer = this.client.newOrderer(ordererUri);
    this.channel.addOrderer(orderer);
    console.debug(`Connected to orderer: ${ordererUri}`);

    // Initialized key store
    Client.newDefaultKeyValueStore({path: store_path})
    .then((state_store) => {
        this.client.setStateStore(state_store);
        let crypto_suite = Client.newCryptoSuite();
        let crypto_store = Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        this.client.setCryptoSuite(crypto_suite);
    })
    .catch((err) => console.error(err));
}

/**
 * loadUser before perform any other tasks.
 * @param {*} opt 
 */
SettlementService.prototype.loadUser = function(opt){
    return this.client.createUser(opt)
        .then((user) => {
            this.user = user;
            return user;
        });
}

/**
 * getTransaction by `key` 
 * @param {*} key 
 */
SettlementService.prototype.getTransaction = function(key){
    let request = {
        chaincodeId: chaincodeId,
        fcn: getTransactionFnc,
        args: [key]
    };

    return this.channel.queryByChaincode(request)
            .then((payload) => {
                console.debug(`Retrieve data for key: ${key}`);
                return payload.toString('utf8');
            });
}

SettlementService.prototype.queryByRange = function(start, end){
    let request = {
        chaincodeId: chaincodeId,
        fcn: queryByDateFnc,
        args: [start, end]
    };

    return this.channel.queryByChaincode(request)
            .then((payload) => {
                console.debug(`Retreive data for range ${start} - ${end}`);
                return payload.toString('utf8');
            });
}

SettlementService.prototype.settleTransaction = function(key){
    let tx_id = this.client.newTransactionID();
    let txIdStr = tx_id.getTransactionID();

    let request = {
        chaincodeId: chaincodeId,
        fcn: settleTransactionFnc,
        args: [key]
    };

    return new Promise((resolve, reject) => {
        this.channel.sendTransactionProposal(request)
        .then((results) => {
            let proposalResponses = results[0];
            let proposal = results[1];
            let isGood = false;

            if (proposalResponses && proposalResponses[0].response &&
                proposalResponses[0].response.status === 200){
                    isGood = true;
            } else {
                console.log(`Settlement transaction proposals were rejected: ${txIdStr}`);
                reject('Transaction proposal was rejected');
            }

            if (isGood){
                let request = {
                    proposalResponses: proposalResponses,
                    proposal: proposal
                };

                console.log(`Commiting transaction: ${txIdStr}`);
                return this.channel.sendTransaction(request);
            }
        })
        .then((results) => {
            if (results.status === 'SUCCESS'){
                console.log(`Successfully commited: ${txIdStr}`);
                resolve(txIdStr);
            } else {
                console.log(`Failed to commit: ${txIdStr}`);
                reject('Failed to commit transaction');
            }
        });
    });
}


module.exports = SettlementService;

