const express = require('express');
const settlementHandler = require('./handler/settlement');

const router = express.Router();

router.post('/settlements/:channelId/:key', settlementHandler.settleTransactionHandler);
router.get('/settlements/channels', settlementHandler.getChannelListHandler);
router.get('/settlements/:channelId/transactions', settlementHandler.getTransactionsByChannelHandler);

module.exports = router;